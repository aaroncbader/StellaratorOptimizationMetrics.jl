using Random

@testset "BallooningStability tests" begin
Random.seed!(0)
## Load global things
rtol = 1.0E-12
rtol_hi = 1.0E-4
rtol_med = 2.0E-8
rtol_very_hi = 1.0E-2

# Example vmec
s = 0.2
vmecname = joinpath(@__DIR__, "wout_ku4.nc")
vmec = VMEC.readVmecWout(vmecname);
vmecSurface = VmecSurface(s, vmec);
nfp = vmecSurface.nfp;
iota = vmecSurface.iota[1];

FourierGridMat = StellaratorOptimizationMetrics.BallooningStability.FourierGridMat
ChebyshevGridMat = StellaratorOptimizationMetrics.BallooningStability.ChebyshevGridMat
FourierMat = StellaratorOptimizationMetrics.BallooningStability.FourierMat
ChebyshevMat = StellaratorOptimizationMetrics.BallooningStability.ChebyshevMat
dFourierMat = StellaratorOptimizationMetrics.BallooningStability.dFourierMat
dChebyshevMat = StellaratorOptimizationMetrics.BallooningStability.dChebyshevMat
ddFourierMat = StellaratorOptimizationMetrics.BallooningStability.ddFourierMat
ddChebyshevMat = StellaratorOptimizationMetrics.BallooningStability.ddChebyshevMat
BallooningCoefInterp = StellaratorOptimizationMetrics.BallooningStability.BallooningCoefInterp
eval_coefs = StellaratorOptimizationMetrics.BallooningStability.eval_coefs
ballooning_coefficient_grid = StellaratorOptimizationMetrics.BallooningStability.ballooning_coefficient_grid
map_α_inv = StellaratorOptimizationMetrics.BallooningStability.map_α_inv
map_α = StellaratorOptimizationMetrics.BallooningStability.map_α
BalloonParams = StellaratorOptimizationMetrics.BallooningStability.BalloonParams
coeffs = StellaratorOptimizationMetrics.BallooningStability.coeffs
coeffs_struct = StellaratorOptimizationMetrics.BallooningStability.coeffs_struct
order_input = StellaratorOptimizationMetrics.BallooningStability.order_input
get_Nζ = StellaratorOptimizationMetrics.BallooningStability.get_Nζ
get_Nα = StellaratorOptimizationMetrics.BallooningStability.get_Nα
get_nfp = StellaratorOptimizationMetrics.BallooningStability.get_nfp
get_iota = StellaratorOptimizationMetrics.BallooningStability.get_iota
summodosd = StellaratorOptimizationMetrics.BallooningStability.summodosd
ddcoeffs_struct = StellaratorOptimizationMetrics.BallooningStability.ddcoeffs_struct
ddeval_coefs = StellaratorOptimizationMetrics.BallooningStability.ddeval_coefs
coeffs_derivs! = StellaratorOptimizationMetrics.BallooningStability.coeffs_derivs!
get_λx = StellaratorOptimizationMetrics.BallooningStability.get_λx
get_λx_derivs = StellaratorOptimizationMetrics.BallooningStability.get_λx_derivs
eig_opt = StellaratorOptimizationMetrics.BallooningStability.eig_opt
cobra = StellaratorOptimizationMetrics.cobra
cobra_opt = StellaratorOptimizationMetrics.BallooningStability.cobra_opt

## summodosd.jl
@testset "methods to obtain physics information (c2, κ, bsuppar) in summodosd.jl" begin
    # This function tests the consistency of the functions summodosd and
    # ballooning_coefficient_grid. If they are consistent with each other and
    # the reference value, then we are happy.

    Nζ = 10;
    Nα = 10;

    ζs = (0:Nζ-1) .* (2π/Nζ);
    αvec = (0:Nα-1) .* (2π/Nα);

    X = 0.9

    c2_coefs, κ_coefs, bsuppar = ballooning_coefficient_grid(vmecSurface, ζs, αvec)
    c2 = c2_coefs[:, :, 1] + c2_coefs[:, :, 2] .* X + c2_coefs[:, :, 3] .* X^2;
    κ  = κ_coefs[:, :, 1]  + κ_coefs[:, :, 2]  .* X;

    c2summodosd = zeros(Nα, Nζ);
    κsummodosd = zeros(Nα, Nζ);
    bsupparsummodosd = zeros(Nα, Nζ);

    vmecCoords = VmecCoordinates(vmecSurface.s, 0, 0)
    pestCoords = PestFromVmec()(vmecCoords, vmecSurface)
    ψpest = pestCoords.ψ
    for iα = 1:Nα, iζ = 1:Nζ
      ζ = ζs[iζ];
      pestCoords  = PestCoordinates(ψpest, αvec[iα], -(ζ - X));
      vmecCoords  = VmecFromPest()(pestCoords,vmecSurface)

      θ = vmecCoords.θ;

      c2i = [0.0];
      κi = [0.0];
      bsuppari = [0.0];
      b2i = [0.0];
      summodosd(vmecSurface, ζ-X, θ, 1, [X], c2i, κi, bsuppari, b2i)

      c2summodosd[iα, iζ] = c2i[1]/b2i[1];
      κsummodosd[iα, iζ] = κi[1];
      bsupparsummodosd[iα, iζ] = bsuppari[1];
    end
    @test norm(c2-c2summodosd)/norm(c2) < rtol_hi
    @test norm(κ-κsummodosd)/norm(κ) < rtol_hi
    @test norm(bsuppar-bsupparsummodosd)/norm(bsuppar) < rtol_hi

end

## BalloonCoefInterp.jl
@testset "Testing that the interpolation works (BalloonCoefInterp.jl, coeffs.jl)" begin
    # Check that the Fourier and Chebyshev functions work as expected
    Nζ = 4;
    Nα = 3;
    ϵ = 1e-7

    AF     = FourierGridMat(Nα; backward=false);
    AFinvT = FourierGridMat(Nα; backward=true);
    AC     = ChebyshevGridMat(Nζ; backward=false);
    ACinvT = ChebyshevGridMat(Nζ; backward=true);

    @test (norm(AFinvT'*AF-I)) < rtol
    @test (norm(ACinvT'*AC-I)) < rtol

    # Check Chebyshev derivatives
    x = 0.25 .* (-4:4);
    dx = ones(9); dx[end] = -1;
    dx = dx .* ϵ;
    x2 = x + dx;
    θ = acos.(x);
    θ2 = acos.(x2);
    AC = ChebyshevMat(θ, Nζ)
    dAC = dChebyshevMat(θ, Nζ)
    ddAC = ddChebyshevMat(θ, Nζ)
    AC2 = ChebyshevMat(θ2, Nζ)
    dAC2 = dChebyshevMat(θ2, Nζ)

    @test norm(AC2 - AC - Diagonal(dx)*dAC) / (ϵ * norm(AC)) < rtol_hi
    @test norm(dAC2 - dAC - Diagonal(dx)*ddAC) / (ϵ * norm(dAC)) < rtol_hi

    # Check Fourier derivatives
    α = (0:8) .* (2π/9)
    dα = ϵ .* ones(length(α));
    α2 = α + dα
    AF = FourierMat(α, Nζ)
    dAF = dFourierMat(α, Nζ)
    ddAF = ddFourierMat(α, Nζ)
    AF2 = FourierMat(α2, Nζ)
    dAF2 = dFourierMat(α2, Nζ)

    @test norm(AF2 - AF - Diagonal(dα)*dAF) / ϵ < rtol_hi
    @test norm(dAF2 - dAF - Diagonal(dα)*ddAF) / ϵ < rtol_hi

    # Test adaptive interpolation with parameters (should cover the whole file)
    nwells = 10;
    geom_input = true;
    tokamak_input=false;
    mode=1;
    vmec_data = VmecData(vmec)
    (r0, amin, beta0, b0_v) = order_input(vmecSurface);
    params = BalloonParams(nwells, geom_input, tokamak_input, mode, r0, amin, beta0, b0_v)

    for spline in [false, true]
        bci   = BallooningCoefInterp(vmecSurface, params; spline)

        # Test easy-to-test getters
        @test get_nfp(bci) == nfp
        @test get_iota(bci) == iota

        # Test interpolation
        ζs = (π/nfp) .* (-cos.(LinRange(0, π, 5)) .+ 1);
        θinitvec = (0:5) .* (2π/(6));
        c2_interp, κ_interp, bsuppar_interp = eval_coefs(bci, θinitvec, ζs);
        c2, κ, bsuppar = ballooning_coefficient_grid(vmecSurface, ζs, θinitvec);

        # Scale so the coefficients can be compared
        phipc = vmecSurface.phi[2]/(-2*π) #phi derivative
        μ0 = 4*π*1.0e-7
        prespn = 2*vmecSurface.pres[2]*μ0/(params.beta0*params.b0_v^2);     # pressure normalized to p_0
        phipn2 = (phipc/(params.b0_v*params.amin^2))^2;      # perpend. lengths normalized to a
        eps2 = (params.r0/params.amin)^2;
        factor = eps2*params.beta0*prespn/phipn2;

        c2      .= c2 .* ((params.amin^2) * (params.b0_v^2));  # perpend. wave vector squared over mag field
        bsuppar .= params.r0 .* bsuppar ./ params.b0_v;
        κ       .= factor .* κ

        @test norm(c2_interp - c2)/norm(c2) < rtol_hi
        @test norm(κ_interp - κ)/norm(κ) < rtol_hi
        @test norm(bsuppar_interp - bsuppar)/norm(bsuppar) < rtol_hi

        # Check mapping makes a little sense

        # Check that we map between surfaces correctly
        dfα = norm(map_α_inv(bci, map_α(bci, θinitvec)) - θinitvec);
        @test dfα < rtol

        c2_bot, κ_bot, bsuppar_bot = eval_coefs(bci, map_α(bci, θinitvec), [0.0]);
        c2_top, κ_top, bsuppar_top = eval_coefs(bci, θinitvec, [2π/nfp]);

        @test norm(c2_bot-c2_top)/norm(c2_bot) < rtol_hi
        @test norm(κ_bot-κ_top)/norm(κ_bot) < rtol_hi
        @test norm(bsuppar_bot-bsuppar_top)/norm(bsuppar_bot) < rtol_hi

        # Check that we get the right p, q, r, as compared to coeffs
        init_zeta = 0.23;
        init_theta = 0.263;
        npt = 20;
        L = 5π;

        vmecCoords = VmecCoordinates(vmecSurface.s, init_theta, init_zeta)
        pestCoords = PestFromVmec()(vmecCoords, vmecSurface)
        init_alpha = pestCoords.α

        coeff_data = coeffs_struct(npt);
        coeff_data.x .= LinRange(-L, L, npt);

        coeff_data_interp = deepcopy(coeff_data)


        coeffs(vmecSurface, s, init_zeta, init_theta, npt, coeff_data, params)
        coeffs(bci, init_zeta, init_alpha, coeff_data_interp)

        x = coeff_data.x;
        q = coeff_data.q;
        p = coeff_data.p;
        r = coeff_data.r;
        q_interp = coeff_data_interp.q;
        p_interp = coeff_data_interp.p;
        r_interp = coeff_data_interp.r;

        @test norm(q-q_interp)/norm(q) < rtol_hi
        @test norm(p-p_interp)/norm(p) < rtol_hi
        @test norm(r-r_interp)/norm(r) < rtol_hi

        # Check ddeval_coefs
        ζ = 0.2:0.1:0.3
        α = 0:1:3
        dζ = ϵ * randn(length(ζ));
        dα = ϵ * zeros(length(α));
        ζ2 = ζ + dζ;
        α2 = α + dα

        c2, κ, bsuppar, dc2, dκ, dbsuppar, ddc2, ddκ, ddbsuppar = ddeval_coefs(bci, α, ζ)
        c22, κ2, bsuppar2, dc22, dκ2, dbsuppar2, ddc22, ddκ2, ddbsuppar2 = ddeval_coefs(bci, α2, ζ2)

        c2_err = zeros(length(α), length(ζ), 3);
        for ii = 1:3
            c2_err[:,:,ii] = c22[:,:,ii] - c2[:,:,ii] - Diagonal(dα)*dc2[:,:,1,ii] - dc2[:,:,2,ii]*Diagonal(dζ)
        end
        bsuppar_err = bsuppar2 - bsuppar - Diagonal(dα)*dbsuppar[:,:,1] - dbsuppar[:,:,2]*Diagonal(dζ)
        @test norm(c2_err)/ϵ < rtol_hi
        @test norm(bsuppar_err)/ϵ < rtol_hi

        # Check coeffs_derivs
        x = 0:1:2
        nx = length(x)
        ddcoeff_data_1 = ddcoeffs_struct(nx);
        ddcoeff_data_1.x[:] .= x;
        ddcoeff_data_2 = ddcoeffs_struct(nx);
        ddcoeff_data_2.x[:] .= x;
        coeff_data = coeffs_struct(nx);
        coeff_data.x[:] .= x;


        r = [0.1, 0.2];
        ϵ = 1e-7;
        dr = ϵ*randn(2);
        r2 = r + dr;

        coeffs(bci, r[2], r[1], coeff_data)
        coeffs_derivs!(bci, r[2], r[1], ddcoeff_data_1)
        coeffs_derivs!(bci, r2[2], r2[1], ddcoeff_data_2)

        @test norm(coeff_data.p - ddcoeff_data_1.p) < rtol
        @test norm(coeff_data.q - ddcoeff_data_1.q) < rtol
        @test norm(coeff_data.r - ddcoeff_data_1.r) < rtol

        rel_err = (p2, p1, dp) -> norm(p2-p1-dp*dr)/(ϵ*norm(p1))
        @test rel_err(ddcoeff_data_2.p, ddcoeff_data_1.p, ddcoeff_data_1.dp) < rtol_hi
        @test norm(ddcoeff_data_2.q - ddcoeff_data_1.q - ddcoeff_data_1.dq*dr)/ϵ < rtol_hi
        @test norm(ddcoeff_data_2.r - ddcoeff_data_1.r - ddcoeff_data_1.dr*dr)/ϵ < rtol_very_hi

        rel_err = (dp2, dp1, ddp) -> norm(vec(dp2)-vec(dp1)-reshape(ddp,2nx,2)*dr)/(ϵ*norm(dp1))
        @test rel_err(ddcoeff_data_2.dp,ddcoeff_data_1.dp,ddcoeff_data_1.ddp) < rtol_hi
        @test rel_err(ddcoeff_data_2.dq,ddcoeff_data_1.dq,ddcoeff_data_1.ddq) < rtol_hi
        @test rel_err(ddcoeff_data_2.dr,ddcoeff_data_1.dr,ddcoeff_data_1.ddr) < rtol_hi
    end
end


## eig_opt.jl
@testset "Testing eig_opt.jl (decoupled from explicitely Ballooning related stuff)" begin

    @testset "Checking our eigenvalue optimization derivatives work" begin
        # Create random quadratic matrices M, A
        n = 4;
        # dv = [1.1957549136236607, -0.1314670572318487, -0.7826061727965966,
        #       -0.15776160974280096]
        # de = [-1.063295936655106, -0.3592515157635325, 1.0565389563420131]
        dv = randn(n);
        de = randn(n-1);
        A = SymTridiagonal(dv, de);

        M_on = true
        M = M_on ? Diagonal(abs.(randn(n)) .+ 1) : Diagonal(ones(n));

        dA1_on = 1;
        dA2_on = 1;
        dA = [SymTridiagonal(dA1_on .* randn(n), dA1_on .* randn(n-1)),
              SymTridiagonal(dA2_on .* randn(n), dA2_on .* randn(n-1))];

        dM1_on = 1;
        dM2_on = 1;
        dM = [Diagonal(dM1_on .* randn(n)),
              Diagonal(dM2_on .* randn(n))];

        ddA11_on = 1;
        ddA12_on = 1;
        ddA22_on = 1;
        ddA = [SymTridiagonal(ddA11_on .* randn(n), ddA11_on .* randn(n-1)),
               SymTridiagonal(ddA12_on .* randn(n), ddA12_on .* randn(n-1)),
               SymTridiagonal(ddA12_on .* randn(n), ddA12_on .* randn(n-1)),
               SymTridiagonal(ddA22_on .* randn(n), ddA22_on .* randn(n-1))];
        ddA = reshape(ddA, 2, 2);
        ddA[1,2] = ddA[2,1];

        ddM11_on = 1;
        ddM12_on = 1;
        ddM22_on = 1;
        ddM = [Diagonal(ddM11_on .* randn(n)),
               Diagonal(ddM12_on .* randn(n)),
               Diagonal(ddM12_on .* randn(n)),
               Diagonal(ddM22_on .* randn(n))];
        ddM = reshape(ddM, 2, 2);
        ddM[1,2] = ddM[2,1];


        A_fn = (r) -> begin
            A_eval = A + dA'*r + (r'*ddA*r)/2;
            dA_eval = ddA*r .+ dA;
            ddA_eval = deepcopy(ddA);
            (A_eval, dA_eval, ddA_eval)
        end

        M_fn = (r) -> begin
            M_eval = M + dM'*r + (r'*ddM*r)/2;
            dM_eval = ddM*r .+ dM;
            ddM_eval = deepcopy(ddM);
            (M_eval, dM_eval, ddM_eval)
        end

        # Get eigenvalues and their derivatives
        pt = [0.,0];
        A1, dA1, ddA1 = A_fn(pt);
        M1, dM1, ddM1 = M_fn(pt);
        λ1, x1 = get_λx(A1, M1)
        dλ1, dx1, ddλ1, ddx1 = get_λx_derivs(A1, M1, dA1, dM1, ddA1, ddM1, λ1, x1)

        eps = 1e-6;
        dpt = eps .* randn(2);
        A2, dA2, ddA2 = A_fn(pt + dpt);
        M2, dM2, ddM2 = M_fn(pt + dpt);
        λ2, x2 = get_λx(A2, M2);
        dλ2, dx2, ddλ2, ddx2 = get_λx_derivs(A2, M2, dA2, dM2, ddA2, ddM2, λ2, x2)

        # Check we are actually finding eigenvalues
        @test norm((A1 - λ1 * M1)*x1) < rtol
        @test norm((A2 - λ2 * M2)*x2) < rtol

        # Check via finite differences that dλ and ddλ are correct
        @test norm(λ2 - λ1 - dλ1'*dpt)/eps < rtol_hi
        @test norm(dλ2 - dλ1 - ddλ1'*dpt)/eps < rtol_hi
    end

    @testset "Checking we correctly optimize eigenvalues" begin
        nfp = 3;
        Afun = (r) -> begin
            θ = r[1]; ζ = r[2];
            dv = [cos(θ)*sin(nfp*ζ), -5, -5];
            de = [1., 1.];
            ∇dv = [[-sin(θ)*sin(nfp*ζ), 0, 0], [nfp*cos(θ)*cos(nfp*ζ), 0, 0]];
            ∇ev = [[0., 0.], [0., 0.]];
            ∇∇dv = reshape([[-cos(θ)*sin(nfp*ζ), 0, 0],
                            [-nfp*sin(θ)*cos(nfp*ζ), 0, 0],
                            [-nfp*sin(θ)*cos(nfp*ζ), 0, 0],
                            [-nfp^2*cos(θ)*sin(nfp*ζ), 0, 0]], 2, 2);
            ∇∇ev = reshape([[0., 0.], [0., 0.], [0., 0.], [0., 0.]], 2, 2);

            A = SymTridiagonal(dv, de);
            dA = [SymTridiagonal(∇dv[ii], ∇ev[ii]) for ii = 1:2];
            ddA = [SymTridiagonal(∇∇dv[ii, jj], ∇∇ev[ii, jj]) for ii = 1:2, jj = 1:2];

            (A, dA, ddA)
        end

        Mfun = (r) -> begin
            M = Diagonal(ones(3));
            dM = [Diagonal(zeros(3)) for ii = 1:2]
            ddM = [Diagonal(zeros(3)) for ii = 1:2, jj = 1:2]

            (M, dM, ddM)
        end

        rmax = [0., π/(2nfp)]
        A, ~, ~ = Afun(rmax)
        M, ~, ~ = Mfun(rmax)
        λmax, xmax = get_λx(A, M);

        r0 = rmax .+ 1e-2 .* [1.,1.];

        r, λ = eig_opt(Afun, Mfun, r0, nfp)
        @test norm(rmax-r) < rtol
        @test norm(λ-λmax) < rtol
    end
end


## cobra.jl
@testset "Test cobra to make sure interp returns same value as non-interp" begin
    nfp = vmec.nfp
    nwells = 10;
    θs = [0.1]
    ζs = [1.1]
    surfs = [s]
    #λ = -0.030178142747899983 # Value measured by λinterp, 8/1/2022
    #println(λ_FC," ",λ_S," ",λ," ",λnoninterp)
    λ = -0.032924746727076126 #adjusted value after renormalizations 4/13/2023
    λ_FC       = cobra(vmec, [s], nwells; θs, ζs, interp=true,spline=false)
    λ_S        = cobra(vmec, [s], nwells; θs, ζs, interp=true,spline=true)
    λnoninterp = cobra(vmec, [s], nwells; θs, ζs, interp=false)

    @test norm(λ_FC .- λ) < rtol_hi
    @test norm(λ_FC - λnoninterp) < rtol_hi
    @test norm(λ_S - λnoninterp) < rtol_hi


    bg, rbg = cobra_opt(vmec, surfs, nwells; θs, ζs)
    bg_cobra = cobra(vmec, surfs, nwells; θs=[rbg[1]], ζs=[rbg[2]*nfp], interp=true)
    # println("bg = $bg, bg_cobra = $bg_cobra, rbg = $rbg")
    @test norm(bg-bg_cobra) < rtol_hi
end

Random.seed!() # Just in case, reseed rand
end
