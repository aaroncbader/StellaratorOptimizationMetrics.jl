module GammaC
using Requires
using StaticArrays
using Interpolations
using LinearAlgebra
using Polyester
using Roots
using QuadGK
using DoubleExponentialFormulas

using PlasmaEquilibriumToolkit

export gamma_c_target

include("Functions.jl")
include("Interfaces.jl")

function __init__()
    @require MPI="da04e1cc-30fd-572f-bb4f-1f8673147195" begin
        include("MPI_Interfaces.jl")
    end
end

end

using .GammaC
