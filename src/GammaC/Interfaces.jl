"""
    gamma_c_target(surfaces, eq, ζMin, ζStep, ζMax, BResolution, epsEff, gammaC)
    gamma_c_target(surfaces, eq, ζMin, ζStep, ζMax, BResolution, comm, epsEff, gammaC)
    gamma_c_target(surf, ζMin, ζStep, ζMax, BResolution, epsEff, gammaC)
    gamma_c_target(surf, ζMin, ζStep, ζMax, BResolution, comm, epsEff, gammaC)

Function to calculate ``\\Gamma_c`` from one or several surfaces (with or without MPI)

# Arguments
 - `surfaces::Union{T, Vector{T}}`: a single float or vector of floats giving the surface at which to compute ``\\Gamma_c``. These values are given in normalized toroidal flux, ``s``
 - `eq::E`: A Vmec or DESC equilibrium
 - `surf::Surface`: A VMEC or DESC surface (code needs either an Equilibrium and a list of surfaces, or a single surface)
 - `ζMin::T`: The toroidal angle value to start the calculation at, in radians
 - `ζStep::T`: The step size to representing the resolution for the calculation, given in radians
 - `ζMax::T`: The toroidal angle to end the calculation at, in radians
 - `BResolution::Int`: The resolution for the B field integration
 - `comm::MPI:Comm`: If included the code will use the MPI interface to speed computation
 - `epsEff::Bool=true`: Optional argument default true. if true, code will calculate and return the value of ``\\epsilon_\\mathrm{eff}``
 - `gammac::Bool=true`: Optional argument default true. if true, code will calculate and return the value of ``\\Gamma_c``

# Outputs:
 - `targetValues::Vector{T}`: an array of values computing ``\\epsilon_\\mathrm{eff}`` and/or ``\\Gamma_c`` depending on the optional bool arguments. In case both are present (default) they are returned in the form in pairs of ``(\\epsilon_\\mathrm{eff}, \\Gamma_c)``
"""
function gamma_c_target(surfaces::Union{T,Vector{T}}, eq::E, ζMin::T, 
                      ζStep::T, ζMax::T,
                      BResolution::Int, epsEff::Bool=true, 
                      gammac::Bool=true) where {T, E <: AbstractMagneticEquilibrium}
  
  nTargetValues = length(surfaces)
  if epsEff && gammac
    targetValues = Array{Tuple{T, T}}(undef, nTargetValues)
  elseif epsEff || gammac
    targetValues = Array{T,1}(undef,nTargetValues)
  else
    println("must select either epsEff or gammac")
  end
  #println(nTargetValues)

  for index = 1:nTargetValues
    surf = magnetic_surface(surfaces[index],eq)

    targetValues[index] = compute_gamma_c(surf,ζMin, ζStep, ζMax, BResolution,epsEff,gammac) 
  end
  return targetValues
end

function gamma_c_target(surf::S,ζMin::T, ζStep::T, ζMax::T,
                      BResolution::Int, epsEff::Bool=true, 
                      gammac::Bool=true) where {T, S <: AbstractSurface}
  if epsEff && gammac
    targetValues = (Inf,Inf)
  elseif epsEff || gammac
    targetValues = Inf
  else
    println("must select either epsEff or gammac")
  end

  return compute_gamma_c(surf,ζMin,ζStep,ζMax,BResolution,epsEff,gammac) 
end

"""
    compute_gamma_c(surf, ζMin, ζStep, ζMax, BResolution, comm, epsEff, gammaC)
    compute_gamma_c(surf, ζMin, ζStep, ζMax, BResolution, epsEff, gammaC)

Helper function for gamma_c_target  

# Arguments
 - `eqSurface::E`: A Magnetic Surfaceobject, currently can support VMEC or DESC surfaces
 - `ζMin::T`: The toroidal angle value to start the calculation at, in radians
 - `ζStep::T`: The step size to representing the resolution for the calculation, given in radians
 - `ζMax::T`: The toroidal angle to end the calculation at, in radians
 - `BResolution::Int`: The resolution for the B field integration
 - `comm:MPI.Comm`: MPI.Comm object. if present, the calculation will be parallelized
 - `epsEff::Bool=true`: Optional argument default true. if true, code will calculate and return the value of ``\\epsilon_\\mathrm{eff}``
 - `gammac::Bool=true`: Optional argument default true. if true, code will calculate and return the value of ``\\Gamma_c``

# Outputs:
 - `target::Vector{T}`: ``\\epsilon_\\mathrm{eff}`` and/or ``\\Gamma_c`` depending on the optional bool arguments. In the default case both are returned in the form in a pair of ``(\\epsilon_\\mathrm{eff}, \\Gamma_c)``
"""


function compute_gamma_c(surf::S,
                         ζMin::A,
                         ζStep::A,
                         ζMax::A,
                         BResolution::Int,
                         epsEff::Bool = true,
                         gammac::Bool = true;
                        ) where {S <: AbstractSurface, A}
  T = Float64
  H2overI_b=Vector{T}(undef, BResolution)
  gammac_b=Vector{T}(undef, BResolution)
  bprime_b=Vector{T}(undef, BResolution)

  dsOverB, gradψOverB = compute_gamma_c!(H2overI_b,gammac_b,bprime_b,
                                         surf,ζMin,ζStep,ζMax,
                                         BResolution,1,BResolution)

  if epsEff
    H2overI = H2overIIntegral(bprime_b, H2overI_b)
    EpsEff32 = H2overI/(gradψOverB)^2*dsOverB * pi * surf.Rmajor_p^2/8/sqrt(2)
  end

  if gammac
    gammacInt = gammacIntegral(bprime_b, gammac_b)
    bigGammac = pi / 2 / sqrt(2) * gammacInt / dsOverB
  end

  if epsEff && gammac
    target = (EpsEff32^(2.0/3), bigGammac)
  elseif epsEff
    target = EpsEff32^(2.0/3)
  else
    target = bigGammac
  end
  return target
end

function compute_gamma_c!(H2overI_b::AbstractVector{T},
                          gammac_b::AbstractVector{T},
                          bprime_b::AbstractVector{T},
                          surf::S,
                          ζMin::A,
                          ζStep::A,
                          ζMax::A,
                          BResolution::Int,
                          firstIndex_b::Int,
                          lastIndex_b::Int,
                         ) where {T,A,S <: AbstractSurface}
  
  ζRange = ζMin:ζStep:ζMax

  B, Bᵛ, dBdψ, ∇ψ, eθ, κ, Vₜ, Bmag = setup_gamma_c_splines(surf,ζRange)

  #calculate all the extrema
  ζExtreme = allExtrema(B,first(ζRange),last(ζRange),step(ζRange)/4)

  (Bmin, BTargetRange) = ConstructBTargetRange(Bmag, BResolution)

  #Integrate quantities over the full length for normalizations
  dsOverB = IntdsOverB(Bᵛ, ζRange[1], ζRange[end])
  gradψOverB = IntGradψOverB(Bᵛ, ∇ψ, ζRange[1], ζRange[end])
 
  #Loop over all the Btargets
  #for (bi, Btarget) in enumerate(BTargetRange)
  @batch for bi in firstIndex_b:lastIndex_b
    Btarget = BTargetRange[bi]
    #calculate locations of wells
    wellBounds = calculateWells(ζExtreme, Btarget, B)

    #wellBounds returns a list of tuples of left bounds, right bounds and minima
    gradψMin = map(w->∇ψ(w[3]),wellBounds)
    eθMin = map(w->eθ(w[3]),wellBounds)

    bprime = Btarget/Bmin
    bprime_b[bi] = bprime

    H2overI_b[bi], gammac_b[bi] = integralTargets(B,Bᵛ,∇ψ,κ,dBdψ,Vₜ,wellBounds,Bmin,bprime,gradψMin,eθMin)
  end

  return dsOverB, gradψOverB
end

