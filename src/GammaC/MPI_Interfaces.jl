using .MPI

function gamma_c_target(surfaces::Union{T,Vector{T}},
                        eq::E,
                        zetaMin::T, 
                        zetaStep::T, 
                        zetaMax::T,
                        BResolution::Int,
                        comm::MPI.Comm,
                        epsEff::Bool=true, 
                        gammac::Bool=true;
                       ) where {T, E <: AbstractMagneticEquilibrium} 
  
  mpiRank=MPI.Comm_rank(comm)
  nTargetValues = length(surfaces)
  if epsEff && gammac
    targetValues = Array{Tuple{T, T}}(undef, nTargetValues)
  elseif epsEff || gammac
    targetValues = Array{T,1}(undef,nTargetValues)
  else
    println("must select either epsEff or gammac")
  end
  #println(nTargetValues)

  for index = 1:nTargetValues
    surf = magnetic_surface(surfaces[index],eq)

    targetTemp = gamma_c_target(surf,zetaMin, zetaStep, zetaMax, BResolution, comm,
                                     epsEff, gammac) 
    if mpiRank == 0
      targetValues[index] = targetTemp
    end
  end
  if mpiRank == 0
    return targetValues
  else
    return nothing
  end
end

function gamma_c_target(surf::S,
                        zetaMin::T,
                        zetaStep::T,
                        zetaMax::T,
                        BResolution::Int,
                        comm::MPI.Comm,
                        epsEff::Bool=true,
                        gammac::Bool=true;
                       ) where {S <: AbstractSurface, T}
  if epsEff && gammac
    targetValues = (Inf,Inf)
  elseif epsEff || gammac
    targetValues = Inf
  else
    println("must select either epsEff or gammac")
  end

  res = try
    compute_gamma_c(surface, zetaMin, zetaStep, zetaMax, BResolution, comm, epsEff, gammac) 
  catch
    gamma_c_target(surface, zetaMin, zetaStep, zetaMax, BResolution-1, comm, epsEff, gammac)
  end
  return res
end

function compute_gamma_c(surf::S,
                         zetaMin::A,
                         zetaStep::A,
                         zetaMax::A,
                         BResolution::Int,
                         comm:: MPI.Comm,
                         epsEff::Bool = true,
                         gammac::Bool = true;
                        ) where {S <: AbstractSurface, A}

  #set up the needed MPI information
  mpiRank=MPI.Comm_rank(comm)
  mpiSize=MPI.Comm_size(comm)

  chunkBase = div(BResolution,mpiSize)
  chunkExtra = mod(BResolution,mpiSize)
  #construct the chunkSize and chunkStart arrays
  chunkStarts = Vector{Int}(undef, mpiSize)
  chunkSizes = Vector{Int}(undef, mpiSize)
  chunkStarts[1] = 1
  chunkSizes[1] = chunkBase
  if chunkExtra > 0
    chunkSizes[1] += 1
  end
  for i in 2:mpiSize
    chunkStarts[i] = chunkStarts[i- 1] + chunkSizes[i-1]
    chunkSizes[i] = i <= chunkExtra ? chunkBase + 1 : chunkBase
  end

  start = chunkStarts[mpiRank+1]
  chunkSize = chunkSizes[mpiRank+1]

  H2overI_b=Vector{T}(undef, BResolution)
  gammac_b=Vector{T}(undef, BResolution)
  bprime_b=Vector{T}(undef,BResolution)

  dsOverB, gradPsiOverB = compute_gamma_c!(H2overI_b,gammac_b,bprime_b,
                                           surf,zetaMin,zetaStep,zetaMax,
                                           BResolution,start,start+chunkSize-1)

  #Gather all the results
  if mpiRank == 0
    MPI.Gatherv!(MPI.IN_PLACE, VBuffer(H2overI_b,chunkSizes), 0, comm)
    MPI.Gatherv!(MPI.IN_PLACE, VBuffer(gammac_b,chunkSizes), 0, comm)
    MPI.Gatherv!(MPI.IN_PLACE, VBuffer(bprime_b,chunkSizes), 0, comm)
  else
    MPI.Gatherv!(H2overI_b[start:start+chunkSize-1],nothing,0,comm)
    MPI.Gatherv!(gammac_b[start:start+chunkSize-1],nothing,0,comm)
    MPI.Gatherv!(bprime_b[start:start+chunkSize-1],nothing,0,comm)
  end

  #println("Time to compute integral targets: "*string(looptime))
  MPI.Barrier(comm)
  
  if mpiRank > 0
    return nothing
  end

  if epsEff 
    H2overI = H2overIIntegral(bprime_b, H2overI_b)
    EpsEff32 = H2overI/(gradPsiOverB)^2*dsOverB * pi * surf.Rmajor_p^2/8/sqrt(2)
    #println("epsfactors: ",surf.Rmajor_p," ",dsOverB," ",gradPsiOverB)
    #println(EpsEff32," ",EpsEff32^(2.0/3))
  end

  if gammac 
    gammacInt = gammacIntegral(bprime_b, gammac_b)
    bigGammac = pi / 2 / sqrt(2) * gammacInt / dsOverB
  end

  if epsEff && gammac 
    target = (EpsEff32^(2.0/3), bigGammac)
  elseif epsEff
    target = EpsEff32^(2.0/3)
  else
    target = bigGammac
  end

  return target
end

