
## Get an interpolant of a surface.
#   Nζ is the degree of the polynomial interpolant minus 1
#   Nα is the degree of the Fourier interpolant
#   specify c2_nodes, κ_nodes, bsuppar_nodes only if you already have the
#     coefficients on a grid (used in adaptive method)
function FCInterp(surf::S, Nζ::Integer, Nα::Integer;
                  c2_nodes=[], κ_nodes = [], bsuppar_nodes = []
                  ) where {S <: AbstractSurface}
  nfp = surf.nfp;
  iota = surf.iota[1];

  if c2_nodes == []
    ζs = (π/nfp) .* (-cos.(LinRange(0, π, Nζ+1)) .+ 1);
    αvec = (0:2Nα+1) .* (2π/(2Nα+2)); # only really need 2Nα+1 points, but an additional is used for nesting
    c2_nodes, κ_nodes, bsuppar_nodes = ballooning_coefficient_grid(surf, ζs, αvec)
  end

  AFinvT = FourierGridMat(Nα; backward=true)
  ACinvT = ChebyshevGridMat(Nζ; backward=true)

  c2 = zeros(1 + 2Nα, Nζ + 1, 3);
  for ii = 1:3
    c2[:,:,ii] = AFinvT' * c2_nodes[:, :, ii] * ACinvT;
  end

  κ = zeros(1 + 2Nα, Nζ + 1, 2);
  for ii = 1:2
    κ[:,:,ii] = AFinvT' * κ_nodes[:, :, ii] * ACinvT;
  end

  bsuppar = AFinvT' * bsuppar_nodes * ACinvT;

  return FCInterp(c2, κ, bsuppar, nfp, iota)
end

function scale_bci(bci::FCInterp, surf::S, params::BalloonParams
                  ) where {S <: AbstractSurface}
  phipc = surf.phi[2]/(-2*π) #phi derivative
  μ0 = 4*π*1.0e-7
  prespn = 2*surf.pres[2]*μ0/(params.beta0*params.b0_v^2);     # pressure normalized to p_0
  phipn2 = (phipc/(params.b0_v*params.amin^2))^2;      # perpend. lengths normalized to a
  eps2 = (params.r0/params.amin)^2;
  factor = eps2*params.beta0*prespn/phipn2;

  bci.c2      .= bci.c2 .* ((params.amin^2) * (params.b0_v^2));  # perpend. wave vector squared over mag field
  bci.bsuppar .= params.r0 .* bci.bsuppar ./ params.b0_v;
  bci.κ       .= factor .* bci.κ
end




function get_c2(bci::FCInterp)
  return bci.c2
end

function get_κ(bci::FCInterp)
  return bci.κ
end

function get_bsuppar(bci::FCInterp)
  return bci.bsuppar
end

function get_Nα(bci::FCInterp)
  return size(get_bsuppar(bci), 1) ÷ 2
end

function get_Nζ(bci::FCInterp)
  return size(get_bsuppar(bci), 2)-1
end

function eval_kernel(bci::FCInterp, coefs, AF, AC)
  return (AF * coefs) * AC'
end



## eval_coefs:
# Get the values of c2_coefs, κ_coefs, and bsuppar_coefs on a grid defined
# by α and ζ
function eval_coefs(bci::FCInterp, α::AbstractVector, ζ::AbstractVector;
                    eval_c2::Bool=true, eval_κ::Bool=true, eval_bsuppar::Bool=true)
  # TODO: should add a check that ζ is in range
  AF = FourierMat(α, get_Nα(bci));
  θ = acos.(1 .- ζ./(π/get_nfp(bci)));
  AC = ChebyshevMat(θ, get_Nζ(bci));

  nα = length(α);
  nζ = length(ζ);

  c2_nodes = []; κ_nodes = []; bsuppar_nodes = [];

  if eval_c2
    c2_nodes = zeros(nα, nζ, 3)
    c2 = get_c2(bci);
    for ii = 1:3
      c2_nodes[:,:,ii] = (AF * c2[:,:,ii]) * AC';
    end
  end

  if eval_κ
    κ_nodes  = zeros(nα, nζ, 2)
    κ = get_κ(bci);
    for ii = 1:2
      κ_nodes[:,:,ii] = (AF * κ[:,:,ii]) * AC';
    end
  end

  if eval_bsuppar
    bsuppar = get_bsuppar(bci);
    bsuppar_nodes = (AF * bsuppar) * AC';
  end

  return c2_nodes, κ_nodes, bsuppar_nodes
end


function ddeval_kernel(coefs, AF, dAF, ddAF, AC, dAC, ddAC)
  # TODO: should add a check that ζ is in range
  n1 = size(AF, 1); n2 = size(AC, 1);
  df = zeros(n1, n2, 2);
  ddf = zeros(n1, n2, 2, 2);

  f            = (AF * coefs) * AC';
  df[:,:,1]    = (dAF * coefs) * AC';
  df[:,:,2]    = (AF * coefs) * dAC';
  ddf[:,:,1,1] = (ddAF * coefs) * AC';
  ddf[:,:,2,1] = (dAF * coefs) * dAC';
  ddf[:,:,2,2] = (AF * coefs) * ddAC';
  ddf[:,:,1,2] = ddf[:,:,2,1];

  return f, df, ddf
end

## ddeval_coefs:
# Get the values of c2_coefs, κ_coefs, and bsuppar_coefs and their derivatives
# w.r.t. α and ζ on a grid defined by α and ζ
function ddeval_coefs(bci::FCInterp, α::AbstractVector, ζ::AbstractVector;
                      eval_c2::Bool=true, eval_κ::Bool=true, eval_bsuppar::Bool=true)
  nfp = get_nfp(bci);
  L = 2π/nfp;
  AF = FourierMat(α, get_Nα(bci));
  dAF = dFourierMat(α, get_Nα(bci));
  ddAF = ddFourierMat(α, get_Nα(bci));
  θ = acos.(1 .- ζ./(π/get_nfp(bci)));
  AC   =               ChebyshevMat(θ, get_Nζ(bci));
  dAC  = (-2/L)   .*  dChebyshevMat(θ, get_Nζ(bci));
  ddAC = (-2/L)^2 .* ddChebyshevMat(θ, get_Nζ(bci));

  nα = length(α);
  nζ = length(ζ);

  c2, dc2, ddc2 = [], [], []
  if eval_c2
    c2 = zeros(nα, nζ, 3)
    dc2 = zeros(nα, nζ, 2, 3)
    ddc2 = zeros(nα, nζ, 2, 2, 3)
    c2_coef = get_c2(bci);
    for ii = 1:3
      c2[:,:,ii], dc2[:,:,:,ii], ddc2[:,:,:,:,ii] = (
                    ddeval_kernel(c2_coef[:,:,ii], AF, dAF, ddAF, AC, dAC, ddAC));
    end
  end

  κ, dκ, ddκ = [], [], []
  if eval_κ
    κ  = zeros(nα, nζ, 2)
    dκ = zeros(nα, nζ, 2, 2)
    ddκ = zeros(nα, nζ, 2, 2, 2)
    κ_coef = get_κ(bci);
    for ii = 1:2
      κ[:,:,ii], dκ[:,:,:,ii], ddκ[:,:,:,:,ii] = (
                     ddeval_kernel(κ_coef[:,:,ii], AF, dAF, ddAF, AC, dAC, ddAC));
    end
  end

  bsuppar, dbsuppar, ddbsuppar = [], [], []
  if eval_bsuppar
    bsuppar_coef = get_bsuppar(bci);
    bsuppar, dbsuppar, ddbsuppar = (
                       ddeval_kernel(bsuppar_coef, AF, dAF, ddAF, AC, dAC, ddAC));
  end

  return c2, κ, bsuppar, dc2, dκ, dbsuppar, ddc2, ddκ, ddbsuppar
end

function FC_refinement_init()
  return 10, 10
end

function FC_refinement_grid(Nα, Nζ, nfp)
  αvec = (0:2Nα+1) .* (2π/(2Nα+2));
  ζs = (π/nfp) .* (-cos.(LinRange(0, π, Nζ+1)) .+ 1);

  return αvec, ζs
end

function FC_refinement_increment(Nα, Nζ)
  return 2Nα+1, 2Nζ
end
