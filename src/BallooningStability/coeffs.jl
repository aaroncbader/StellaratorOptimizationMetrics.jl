# function scale_coefficients!(bsuppar, b2, c2, k_s, surf::S, params::BalloonParams)
#   phipc = surf.phi[2]/(-2*π) #phi derivative
#
#   b2[:] = b2[:] ./ (params.b0_v^2);                    # magnetic field normalized to B_0
#   bsuppar[:] = params.r0*bsuppar[:] ./ params.b0_v;
#   c2[:] = c2[:] .* (params.amin^2);                    # perpend. wave vector squared
#   μ0 = 4*π*1.0e-7
#   prespn = 2*surf.pres[2]*μ0/(params.beta0*params.b0_v^2);     # pressure normalized to p_0
#   phipn2 = (phipc/(params.b0_v*params.amin^2))^2;      # perpend. lengths normalized to a
#   eps2 = (params.r0/params.amin)^2;
#   factor = eps2*params.beta0*prespn/phipn2;
#   k_s[:] = factor.*k_s[:]
# end

function coeffs(surf::S, s::Float64, init_zeta::Float64, init_theta::Float64,
                npt::Int64, coeff_data::coeffs_struct, params::BalloonParams
               ) where {S <: AbstractSurface}

  b2 = zeros(npt);
  c2 = zeros(npt);
  k_s = zeros(npt);
  bsuppar = zeros(npt);

  lfail_balloon = summodosd(surf, init_zeta, init_theta, npt, coeff_data.x, c2, k_s, bsuppar, b2);
  if lfail_balloon
     return lfail_balloon
  end

  phipc = surf.phi[2]/(-2*π) #phi derivative

  b2n = b2 ./ (params.b0_v^2);                    # magnetic field normalized to B_0
  bsupparn = params.r0*bsuppar ./ params.b0_v;
  c2n = c2 .* (params.amin^2);                    # perpend. wave vector squared
  μ0 = 4*π*1.0e-7
  prespn = 2*surf.pres[2]*μ0/(params.beta0*params.b0_v^2);     # pressure normalized to p_0
  phipn2 = (phipc/(params.b0_v*params.amin^2))^2;      # perpend. lengths normalized to a
  eps2 = (params.r0/params.amin)^2;
  factor = eps2*params.beta0*prespn/phipn2;

  # P, Q and R:  ballooning coefficients

  coeff_data.p .= bsupparn .* c2n ./ b2n;
  coeff_data.q .= factor .* k_s ./ bsupparn;
  coeff_data.r .= c2n ./ (b2n .* bsupparn);

  #debugging
  #zetang = init_zeta .+ coeff_data.x;
  #for i in 1:length(coeff_data.p)
  #  println("coeffs: ",zetang[i]," ",coeff_data.p[i]," ",coeff_data.q[i]," ",coeff_data.r[i])
  #end

  return lfail_balloon
end

# Same function, except gets the coefficients from the interpolant
function coeffs(bci::BallooningCoefInterp, init_zeta::Float64,
                init_alpha::Float64, coeff_data::coeffs_struct;
                eval_r::Bool=true, eval_q::Bool=true, eval_p::Bool=true)
  nfp = get_nfp(bci);
  iota = get_iota(bci);
  T = 2π/nfp

  x = coeff_data.x
  ζ = init_zeta .+ x;
  ζperiod = div.(ζ, T, RoundDown);
  ps = unique(ζperiod);
  αs = init_alpha .+ (2π*iota/nfp).*ps;

  eval_c2 = eval_p || eval_r;
  eval_κ = eval_q;
  eval_bsuppar = eval_p || eval_q || eval_r;

  for ii = 1:length(ps)
    p = ps[ii];
    α = αs[ii];
    ind = ζperiod .== p;

    c2, κ, bsuppar = eval_coefs(bci, α, ζ[ind] .- p*T)

    eval_c2 && (c2x = c2[:, 1] + c2[:, 2].*x[ind] + c2[:,3] .* (x[ind].^2))
    eval_κ && (κx = κ[:, 1] + κ[:,2].*x[ind])


    eval_p && (coeff_data.p[ind] = bsuppar .* c2x);
    eval_κ && (coeff_data.q[ind] = κx ./ bsuppar);
    eval_bsuppar && (coeff_data.r[ind] = c2x ./ bsuppar);
  end

  return false
end


# Evaluates the coefficients and their derivatives w.r.t. the initial point on a field line
function coeffs_derivs!(bci::BallooningCoefInterp, init_zeta::Float64,
                        init_alpha::Float64, ddcoeff_data::ddcoeffs_struct;
                        eval_r::Bool=true, eval_q::Bool=true, eval_p::Bool=true)
  nfp = get_nfp(bci);
  iota = get_iota(bci);
  T = 2π/nfp

  x = ddcoeff_data.x
  ζ = init_zeta .+ x;
  ζperiod = div.(ζ, T, RoundDown);
  ps = unique(ζperiod);
  αs = similar(ps);

  αs[ps.==0] .= init_alpha;

  α = init_alpha;
  for ii = 1:maximum(ps)
    α = map_α(bci,α)
    αs[ps .== ii] .= α;
  end

  α = init_alpha;
  for ii = -1:-1:minimum(ps)
    α = map_α_inv(bci, α)
    αs[ps.==ii] .= α;
  end

  eval_c2 = eval_p || eval_r;
  eval_κ = eval_q;
  eval_bsuppar = eval_p || eval_q || eval_r;

  for ii = 1:length(ps)
    p = ps[ii];
    α = αs[ii];
    ind = ζperiod .== p;
    nζ = sum(ind);

    c2, κ, bsuppar, dc2, dκ, dbsuppar, ddc2, ddκ, ddbsuppar = (
            ddeval_coefs(bci, α, ζ[ind] .- p*T; eval_c2, eval_κ, eval_bsuppar))

    eval_c2 && (c2x = c2[:, 1] + c2[:, 2].*x[ind] + c2[:,3] .* (x[ind].^2))
    eval_κ && (κx = κ[:, 1] + κ[:,2].*x[ind])

    dc2x = zeros(nζ, 2)
    dκx = zeros(nζ, 2);

    for ii = 1:2
      eval_c2 && (dc2x[:, ii] = dc2[:, ii, 1] + dc2[:, ii, 2].*x[ind] + dc2[:, ii, 3] .* (x[ind].^2))
      eval_κ && (dκx[:, ii] = dκ[:, ii, 1] + dκ[:, ii, 2].*x[ind]);
    end

    ddc2x = zeros(nζ, 2, 2)
    ddκx = zeros(nζ, 2, 2);
    for ii = 1:2, jj = 1:2
      eval_c2 && (ddc2x[:, ii, jj] = ddc2[:, ii, jj, 1] + ddc2[:, ii, jj, 2].*x[ind] + ddc2[:, ii, jj, 3] .* (x[ind].^2));
      eval_κ && (ddκx[:, ii, jj] = ddκ[:, ii, jj, 1] + ddκ[:, ii, jj, 2].*x[ind]);
    end

    if eval_p
      ddcoeff_data.p[ind] = bsuppar .* c2x;

      for ii = 1:2
        ddcoeff_data.dp[ind, ii] = dbsuppar[:, ii] .* c2x + bsuppar .* dc2x[:, ii]
      end

      for ii = 1:2, jj = 1:2
        ddcoeff_data.ddp[ind, ii, jj] = (ddbsuppar[:, ii, jj] .* c2x +
                                         dbsuppar[:, ii] .* dc2x[:, jj] +
                                         dbsuppar[:, jj] .* dc2x[:, ii] +
                                         bsuppar .* ddc2x[:, ii, jj])
      end
    end

    if eval_q
      ddcoeff_data.q[ind] = κx ./ bsuppar;

      for ii = 1:2
        ddcoeff_data.dq[ind, ii] = dκx[:, ii] ./ bsuppar - dbsuppar[:, ii] .* κx ./ bsuppar.^2;
      end

      for ii = 1:2, jj = 1:2
        ddcoeff_data.ddq[ind, ii, jj] = (ddκx[:, ii, jj] ./ bsuppar -
                                    dbsuppar[:, ii] .* dκx[:, jj] ./ bsuppar.^2 -
                                    dbsuppar[:, jj] .* dκx[:, ii] ./ bsuppar.^2 -
                                    ddbsuppar[:, ii, jj] .* κx ./ bsuppar.^2 +
                                    2 .* κx .* dbsuppar[:, ii] .* dbsuppar[:,jj] ./ bsuppar.^3)
      end
    end

    if eval_r
      ddcoeff_data.r[ind] = c2x ./ bsuppar;

      for ii = 1:2
        ddcoeff_data.dr[ind, ii] = dc2x[:, ii] ./ bsuppar - dbsuppar[:, ii] .* c2x ./ bsuppar.^2;
      end

      for ii = 1:2, jj = 1:2
        ddcoeff_data.ddr[ind, ii, jj] = (ddc2x[:, ii, jj] ./ bsuppar -
                                    dbsuppar[:, ii] .* dc2x[:, jj] ./ bsuppar.^2 -
                                    dbsuppar[:, jj] .* dc2x[:, ii] ./ bsuppar.^2 -
                                    ddbsuppar[:, ii, jj] .* c2x ./ bsuppar.^2 +
                                    2 .* c2x .* dbsuppar[:, ii] .* dbsuppar[:,jj] ./ bsuppar.^3)
      end
    end
  end

  return false
end
