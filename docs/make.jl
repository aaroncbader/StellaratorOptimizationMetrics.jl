using StellaratorOptimizationMetrics
using Documenter

DocMeta.setdocmeta!(StellaratorOptimizationMetrics, :DocTestSetup, :(using StellaratorOptimizationMetrics); recursive=true)

makedocs(;
    modules=[StellaratorOptimizationMetrics],
    authors="Benjamin Faber <bfaber@wisc.edu> and Aaron Bader <abader@engr.wisc.edu> and Sanket Patil <sapatil@wisc.edu>",
    repo="https://gitlab.com/WISTELL/StellaratorOptimizationMetrics.jl/blob/{commit}{path}#{line}",
    sitename="StellaratorOptimizationMetrics.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://wistell.gitlab.io/StellaratorOptimizationMetrics.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
        "Library" => [
                      "Private" => "library/internals.md",
                     ],
    ],
)
